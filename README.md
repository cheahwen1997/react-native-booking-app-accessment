# react-native-booking-app-accessment

## Software

- VSCode/etc
- Android Studio/XCode
- Git
- Nvm
- Npm/etc

## Download & Setup

```
git clone https://gitlab.com/cheahwen1997/react-native-booking-app-accessment
cd react-native-booking-app-accessment
nvm install 20.11.1  # if no install
nvm use
npm i
npm run tsc
```

## Run on Android

```
npm run android
```

## Run on Ios

```
npm run ios
```

## Folder Structure

> src/api: Specify any Rest Client class singleton

> src/components: Specify any Reusable Components

> src/configs: Specify any low risk config data

> src/data: Specify any constant data

> src/hooks: Specify any custom hooks

> src/screens: Specify any screens/pages

> src/screens: Specify any custom types

> src/utils: Specify any common utils

> src/Router.tsx: Specify any routes

> App.tsx: Root entry

## Sample UI

### Task 1. Booking List

![Booking List](assets/docs/booking-list.png)

### Task 2. User Profile & User Booking

![User Profile](assets/docs/user-profile.png)

### Task 3. Booking Detail Page

![Booking Detail](assets/docs/booking-detail.png)

### Task 4. Create Booking Page

![Edit Booking](assets/docs/create-booking.png)

### Task 5. Edit Booking Page

![Alt text](assets/docs/edit-booking.png)
![Alt text](assets/docs/edit-success.png)
