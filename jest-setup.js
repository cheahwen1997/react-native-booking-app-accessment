jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      dispatch: jest.fn(),
      navigate: jest.fn(),
      addListener: jest.fn()
    }),
    useRoute: () => ({
      params: {
        id: undefined
      }
    })
  };
});
