import renderer from 'react-test-renderer';
import BookingDetailScreen from './BookingDetailScreen';

describe('Booking Detail Screen', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('performs snapshot testing', () => {
    const tree = renderer.create(<BookingDetailScreen />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
