import {
  ParamListBase,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/core';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { Switch, Text, TextInput, View } from 'react-native';
import DateTimePicker, { DateType } from 'react-native-ui-datepicker';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BookingRestClientInstance } from '../api';
import {
  BadgeID,
  BaseLayout,
  Card,
  FlatButton,
  LoadingOutline
} from '../components';
import { ScreenNames } from '../configs';
import { MockUser } from '../data';
import { useRequest } from '../hooks';
import { BookingDetail } from '../types';

export default function CreateEditBookingScreen() {
  const navigation = useNavigation();
  let route = useRoute<
    RouteProp<ParamListBase> & { params: { id?: number } }
  >();
  const isEdit = !!route.params?.id;

  const [dateRange, setDateRange] = useState<{
    startDate: DateType;
    endDate: DateType;
  }>({
    startDate: undefined,
    endDate: undefined
  });

  const defaultInput = {
    firstname: MockUser.firstname,
    lastname: MockUser.lastname,
    bookingdates: {
      checkin: undefined,
      checkout: undefined
    },
    totalprice: 0,
    depositpaid: false,
    additionalneeds: ''
  };

  const [input, setInput] = useState<BookingDetail>(defaultInput);

  const [submitOk, setSubmitOk] = useState<number>(-1);

  const {
    data: getBookingDetailData,
    loading: getBookingDetailLoading,
    error: getBookingDetailError,
    callApi: callGetBookingDetailApi
  } = useRequest<BookingDetail>(() =>
    BookingRestClientInstance.getBookingDetail(route.params.id ?? -1)
  );

  const {
    data: createBookingData,
    loading: createBookingLoading,
    error: createBookingError,
    callApi: callCreateBookingApi
  } = useRequest<BookingDetail>(() =>
    BookingRestClientInstance.createBooking(input)
  );

  const {
    data: updateBookingData,
    loading: updateBookingLoading,
    error: updateBookingError,
    callApi: callUpdateBookingApi
  } = useRequest<BookingDetail>(() =>
    BookingRestClientInstance.updateBooking(route.params.id ?? -1, input)
  );

  useEffect(() => {
    const listenerUnsubscribe = navigation.addListener('focus', () => {
      if (route.params?.id) {
        callGetBookingDetailApi();
      }
    });
    setInput(defaultInput);
    setSubmitOk(-1);
    setDateRange({
      startDate: undefined,
      endDate: undefined
    });
    return () => listenerUnsubscribe();
  }, [route.params]);

  useEffect(() => {
    if (getBookingDetailData)
      setInput((prev) => ({
        ...prev,
        ...getBookingDetailData
      }));
    setDateRange({
      startDate: getBookingDetailData?.bookingdates?.checkin
        ? new Date(getBookingDetailData?.bookingdates?.checkin)
        : undefined,
      endDate: getBookingDetailData?.bookingdates?.checkout
        ? new Date(getBookingDetailData?.bookingdates?.checkout)
        : undefined
    });
  }, [getBookingDetailData]);

  return (
    <BaseLayout>
      <Card>
        {isEdit && (
          <View className="flex flex-row mb-5">
            <BadgeID label={`ID: ${route.params?.id}`} />
          </View>
        )}
        <View className="flex flex-col space-y-2">
          {isEdit && getBookingDetailLoading && <LoadingOutline />}
          <View className="flex flex-row justify-between items-start">
            <TextInput
              className="basis-5/12 bg-white"
              placeholder="First Name"
              value={input.firstname}
              readOnly
            />
            <TextInput
              className="basis-5/12 bg-white"
              placeholder="Last Name"
              value={input.lastname}
              readOnly
            />
          </View>
          <View className="flex flex-row space-x-10  items-center">
            <TextInput
              className="basis-5/12 bg-white"
              placeholder="Total Price"
              value={String(input.totalprice)}
              keyboardType="numeric"
              onChangeText={(val) => {
                setInput((prev) => ({
                  ...prev,
                  totalprice: parseInt(val)
                }));
              }}
            />
            <View className="basis-1/3 flex flex-row space-y-3 justify-between items-center">
              <Text className="text-white">Deposit Paid: </Text>
              <Switch
                trackColor={{ false: '#767577', true: '#81b0ff' }}
                thumbColor="#fff"
                ios_backgroundColor="#3e3e3e"
                onValueChange={() =>
                  setInput((prev) => ({
                    ...prev,
                    depositpaid: !prev.depositpaid
                  }))
                }
                value={input.depositpaid}
              />
            </View>
          </View>
          <TextInput
            className="bg-white"
            placeholder="Notes"
            value={input.additionalneeds}
            onChangeText={(val) => {
              setInput((prev) => ({
                ...prev,
                additionalneeds: val
              }));
            }}
          />
          <View className="flex flex-row space-x-3 items-center">
            <Text className="text-white font-bold">Stay Duration:</Text>
            <View>
              <BadgeID
                label={`${input.bookingdates?.checkin ?? 'N/A'} - ${
                  input.bookingdates?.checkout ?? 'N/A'
                }`}
              />
            </View>
          </View>
          <View className="bg-white mb-10">
            <DateTimePicker
              mode="range"
              startDate={dateRange.startDate}
              endDate={dateRange.endDate}
              onChange={({ startDate, endDate }) => {
                setDateRange({
                  startDate,
                  endDate
                });
                setInput((prev) => ({
                  ...prev,
                  bookingdates: {
                    checkin: startDate
                      ? moment(startDate.toString()).format('YYYY-MM-DD')
                      : undefined,
                    checkout: endDate
                      ? moment(endDate.toString()).format('YYYY-MM-DD')
                      : undefined
                  }
                }));
              }}
            />
          </View>
          <FlatButton
            onPress={async () => {
              setSubmitOk(-1);
              const success = isEdit
                ? await callUpdateBookingApi()
                : await callCreateBookingApi();
              setSubmitOk(success ? 1 : 0);
            }}
            disabled={createBookingLoading || updateBookingLoading}
            label="Submit"
            buttonClassName={
              createBookingLoading ? 'bg-neutral-400 p-2' : 'bg-sky-400 p-2'
            }
            labelClassName="font-bold"
          />
          {(createBookingLoading || updateBookingLoading) && (
            <Text className="text-neutral-400 italic">Submitting form...</Text>
          )}
          {(!createBookingLoading || !updateBookingLoading) &&
            submitOk !== -1 && (
              <View className="flex flex-row gap-4 justify-center items-center">
                <Text
                  className={
                    submitOk ? 'text-green-400 italic' : 'text-red-400 italic'
                  }>
                  {submitOk
                    ? `${isEdit ? 'Update' : 'Create'} Ok. Your booking ID is ${
                        isEdit
                          ? route.params.id
                          : createBookingData?.bookingid ?? '-'
                      }`
                    : `Error: ${createBookingError?.response?.data}`}
                </Text>
                {submitOk &&
                  (createBookingData?.bookingid || route.params.id) && (
                    <View>
                      <FlatButton
                        onPress={async () => {
                          navigation.navigate({
                            name: ScreenNames.BOOKING_DETAIL,
                            params: {
                              id: isEdit
                                ? route.params.id
                                : createBookingData?.bookingid
                            }
                          } as never);
                        }}
                        icon={
                          <MaterialIcons
                            name="arrow-forward"
                            color="white"
                            size={15}
                          />
                        }
                        disabled={createBookingLoading || updateBookingLoading}
                        label="View"
                        buttonClassName="bg-green-600 p-1 rounded-full"
                        labelClassName="font-bold text-white"
                      />
                    </View>
                  )}
              </View>
            )}
        </View>
      </Card>
    </BaseLayout>
  );
}
