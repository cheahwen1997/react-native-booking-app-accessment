import { useNavigation } from '@react-navigation/native';
import { useEffect, useState } from 'react';
import { FlatList, Text, View } from 'react-native';
import { Avatar } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BookingRestClientInstance } from '../api';
import {
  BadgeID,
  BaseLayout,
  BookingIDCard,
  Card,
  FlatButton,
  LoadingOutline
} from '../components';
import { ScreenNames } from '../configs';
import { MockUser } from '../data';
import { useRequest } from '../hooks';
import { BookingDetail, BookingId } from '../types';

const UserBooking = () => {
  const navigation = useNavigation();

  const [deleteId, setDeleteId] = useState<number | null | undefined>(null);

  const {
    data: getBookingData,
    loading: getBookingLoading,
    error: getBookingError,
    callApi: callGetBookingApi
  } = useRequest<BookingId[]>(() =>
    BookingRestClientInstance.getBookingIds({
      firstname: MockUser.firstname,
      lastname: MockUser.lastname
    })
  );

  const {
    data: deleteBookingDetailData,
    loading: deleteBookingDetailLoading,
    error: deleteBookingDetailError,
    callApi: callDeleteBookingDetailApi
  } = useRequest<BookingDetail>(() => {
    BookingRestClientInstance.deleteBooking(deleteId as number);
  });

  useEffect(() => {
    const listenerUnsubscribe = navigation.addListener('focus', () => {
      callGetBookingApi();
      setDeleteId(null);
    });
    return () => listenerUnsubscribe();
  }, [navigation]);

  useEffect(() => {
    if (!!deleteId)
      callDeleteBookingDetailApi().then(() => {
        callGetBookingApi();
        setDeleteId(null);
      });
  }, [deleteId]);

  return (
    <View>
      <View className="flex flex-row justify-between items-center mb-5">
        <View>
          <Text className="text-white font-bold text-lg">My Booking</Text>
        </View>
        <View className="sticky top-0 flex flex-row justify-end items-center space-x-3">
          <View>
            <FlatButton
              onPress={() => {
                navigation.navigate({
                  name: ScreenNames.CREATE_BOOKING
                } as never);
              }}
              buttonClassName="bg-sky-500 p-1"
              icon={<MaterialIcons name="add" color="white" size={20} />}
            />
          </View>
          <View>
            <FlatButton
              onPress={() => {
                callGetBookingApi();
              }}
              buttonClassName={
                getBookingLoading ? 'bg-neutral-500 p-1' : 'bg-cyan-500 p-1'
              }
              icon={<MaterialIcons name="refresh" color="white" size={20} />}
            />
          </View>
        </View>
      </View>
      {getBookingLoading ? (
        <LoadingOutline />
      ) : getBookingData?.length === 0 ? (
        <Text className="text-neutral-400 italic">
          You don't have any booking yet...
        </Text>
      ) : (
        <FlatList
          data={getBookingData}
          renderItem={({ item }) => {
            return (
              <BookingIDCard
                booking={item}
                isEditable={true}
                isDeletable={true}
              />
            );
          }}
          keyExtractor={(item) => String(item.bookingid)}
        />
      )}
    </View>
  );
};

const Profile = () => {
  return (
    <Card className="flex flex-row space-x-10 bg-gray-900">
      <View className="flex text-center justify-center items-center">
        <Avatar
          size="large"
          rounded
          icon={{ name: 'user', type: 'font-awesome' }}
          activeOpacity={0.7}
          containerStyle={{
            backgroundColor: 'lightgray'
          }}
        />
      </View>
      <View className="flex text-center justify-center">
        <BadgeID label={`ID: ${MockUser.userid}`} />
        <Text className="text-white font-bold mt-2">
          {MockUser.firstname} {MockUser.lastname}
        </Text>
      </View>
    </Card>
  );
};

export default function UserProfileScreen() {
  return (
    <BaseLayout>
      <View className="flex flex-col v-screen space-y-10">
        <View className="flex text-center justify-center">
          <Profile />
        </View>
        <View className="flex text-center justify-center ">
          <UserBooking />
        </View>
      </View>
    </BaseLayout>
  );
}
