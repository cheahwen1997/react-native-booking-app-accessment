import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';
import { FlatList, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BookingRestClientInstance } from '../api';
import {
  BaseLayout,
  BookingIDCard,
  FlatButton,
  LoadingOutline
} from '../components';
import { ScreenNames } from '../configs';
import { useRequest } from '../hooks';
import { BookingId } from '../types';

export default function BookingListScreen() {
  const navigation = useNavigation();

  const { data, loading, error, callApi } = useRequest<BookingId[]>(() =>
    BookingRestClientInstance.getBookingIds()
  );

  useEffect(() => {
    callApi();
  }, []);

  return (
    <BaseLayout>
      <View className="sticky top-0 flex flex-row justify-end items-center space-x-3">
        <View>
          <FlatButton
            onPress={() => {
              navigation.navigate({
                name: ScreenNames.CREATE_BOOKING
              } as never);
            }}
            buttonClassName="bg-sky-500 p-1"
            icon={<MaterialIcons name="add" color="white" size={20} />}
          />
        </View>
        <View>
          <FlatButton
            onPress={() => {
              callApi();
            }}
            disabled={loading}
            buttonClassName={loading ? 'bg-neutral-500 p-1' : 'bg-cyan-500 p-1'}
            icon={<MaterialIcons name="refresh" color="white" size={20} />}
          />
        </View>
      </View>
      {loading ? (
        <LoadingOutline />
      ) : data?.length === 0 ? (
        <Text className="text-neutral-400 italic">
          There's no any booking yet...
        </Text>
      ) : (
        <View>
          <FlatList
            data={data}
            renderItem={({ item }) => {
              return <BookingIDCard booking={item} />;
            }}
            keyExtractor={(item) => String(item.bookingid)}
          />
        </View>
      )}
    </BaseLayout>
  );
}
