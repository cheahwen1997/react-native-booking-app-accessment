import {
  ParamListBase,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/native';
import { useEffect, useState } from 'react';
import { Image, Text, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BookingRestClientInstance } from '../api';
import {
  BadgeError,
  BadgeID,
  BadgeSuccess,
  BaseLayout,
  Card,
  FlatButton,
  LoadingOutline
} from '../components';
import { ScreenNames } from '../configs';
import { MockUser } from '../data';
import { useRequest } from '../hooks';
import { BookingDetail } from '../types';
import { TimeUtils } from '../utils';

export default function BookingDetailScreen() {
  const navigation = useNavigation();
  const route = useRoute<
    RouteProp<ParamListBase> & { params: { id: number } }
  >();
  const id = route.params.id;

  const [isDeletable, setIsDeletable] = useState(false);
  const [isEditable, setIsEditable] = useState(false);

  const {
    data: getBookingDetailData,
    loading: getBookingDetailLoading,
    error: getBookingDetailError,
    callApi: callGetBookingDetailApi
  } = useRequest<BookingDetail>(() =>
    BookingRestClientInstance.getBookingDetail(route.params.id)
  );

  const {
    data: deleteBookingDetailData,
    loading: deleteBookingDetailLoading,
    error: deleteBookingDetailError,
    callApi: callDeleteBookingDetailApi
  } = useRequest<BookingDetail>(() => {
    BookingRestClientInstance.deleteBooking(route.params.id as number);
  });

  useEffect(() => {
    if (getBookingDetailData) {
      if (
        getBookingDetailData.firstname === MockUser.firstname &&
        getBookingDetailData.lastname === MockUser.lastname
      ) {
        setIsEditable(true);
        setIsDeletable(true);
      }
    }
  }, [getBookingDetailData]);

  useEffect(() => {
    const listenerUnsubscribe = navigation.addListener('focus', () => {
      if (route.params?.id) callGetBookingDetailApi();
    });
    return () => listenerUnsubscribe();
  }, [route.params]);

  return (
    <BaseLayout>
      <Card>
        <View className="flex flex-row justify-between items-center mb-10">
          <BadgeID label={`ID: ${id}`} />
          <View className="flex flex-row justify-evenly items-center space-x-3">
            {isEditable && (
              <View>
                <FlatButton
                  onPress={() => {
                    navigation.navigate({
                      name: ScreenNames.EDIT_BOOKING,
                      params: {
                        id: route.params?.id
                      }
                    } as never);
                  }}
                  buttonClassName="bg-green-500 p-1"
                  icon={<MaterialIcons name="create" color="white" size={20} />}
                />
              </View>
            )}
            {isDeletable && (
              <View>
                <FlatButton
                  onPress={() => {
                    callDeleteBookingDetailApi()
                      .then(() => TimeUtils.sleep(500))
                      .then(() => {
                        navigation.goBack();
                      });
                  }}
                  buttonClassName="bg-red-500 p-1"
                  icon={<MaterialIcons name="delete" color="white" size={20} />}
                />
              </View>
            )}
            <View>
              <FlatButton
                onPress={() => {
                  callGetBookingDetailApi();
                }}
                buttonClassName={
                  getBookingDetailLoading
                    ? 'bg-neutral-500 p-1'
                    : 'bg-cyan-500 p-1'
                }
                icon={<MaterialIcons name="refresh" color="white" size={20} />}
              />
            </View>
          </View>
        </View>
        {getBookingDetailLoading ? (
          <LoadingOutline />
        ) : !getBookingDetailData ||
          getBookingDetailError?.response?.status === 404 ? (
          <Text className="text-neutral-400 text-center italic">
            The booking is not longer exist...
          </Text>
        ) : (
          <View className="flex flex-col v-screen space-y-3">
            <Image
              className="w-full h-40"
              source={{
                uri: 'https://i.pinimg.com/originals/a5/25/86/a52586ab1561ad8e9a9f6f7b7b159002.jpg'
              }}
            />
            <View className="flex flex-row space-x-2 justify-end items-center">
              <Text className="flex text-white font-bold text-lg">
                ${getBookingDetailData?.totalprice}
              </Text>
              <View className="flex">
                {getBookingDetailData?.depositpaid ? (
                  <BadgeSuccess label="Paid" />
                ) : (
                  <BadgeError label="Unpaid" />
                )}
              </View>
            </View>
            <Text className="text-white">
              Guest:{' '}
              <Text className="font-bold text-yellow-400">
                {getBookingDetailData?.firstname}{' '}
                {getBookingDetailData?.lastname}{' '}
              </Text>
            </Text>

            <Text className="text-white">
              Stay Duration:{' '}
              <Text className="font-bold text-yellow-400">
                {getBookingDetailData?.bookingdates?.checkin}
              </Text>{' '}
              -{' '}
              <Text className="font-bold text-yellow-400">
                {getBookingDetailData?.bookingdates?.checkout}
              </Text>
            </Text>
            <Text className="text-white">
              Notes:{' '}
              <Text className="font-bold text-yellow-400">
                {getBookingDetailData?.additionalneeds}{' '}
              </Text>
            </Text>
          </View>
        )}
      </Card>
    </BaseLayout>
  );
}
