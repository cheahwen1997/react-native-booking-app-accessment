import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Route, useNavigation } from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { BackButton } from './components';
import { ScreenNames } from './configs';
import BookingDetailScreen from './screens/BookingDetailScreen';
import BookingListScreen from './screens/BookingListScreen';
import CreateEditBookingScreen from './screens/CreateEditBookingScreen';
import UserProfileScreen from './screens/UserProfileScreen';

const Tab = createBottomTabNavigator();

export const ScreenConfigs = {
  BOOKING: {
    name: ScreenNames.BOOKING,
    tabBarVisible: true,
    menuVisible: true,
    goBackVisible: false,
    component: BookingListScreen,
    renderIcon: ({ focused }: { focused: boolean }) => {
      return (
        <MaterialIcons
          name="roofing"
          color={focused ? 'lightblue' : '#fff'}
          size={20}
        />
      );
    }
  },
  BOOKING_DETAIL: {
    name: ScreenNames.BOOKING_DETAIL,
    tabBarVisible: false,
    menuVisible: false,
    goBackVisible: true,
    component: BookingDetailScreen,
    renderIcon: () => null
  },
  USER_PROFILE: {
    name: ScreenNames.USER_PROFILE,
    tabBarVisible: true,
    menuVisible: true,
    goBackVisible: false,
    component: UserProfileScreen,
    renderIcon: ({ focused }: { focused: boolean }) => {
      return (
        <MaterialIcons
          name="face"
          color={focused ? 'lightblue' : '#fff'}
          size={20}
        />
      );
    }
  },
  CREATE_BOOKING: {
    name: ScreenNames.CREATE_BOOKING,
    tabBarVisible: false,
    menuVisible: false,
    goBackVisible: true,
    component: CreateEditBookingScreen,
    renderIcon: () => null
  },
  EDIT_BOOKING: {
    name: ScreenNames.EDIT_BOOKING,
    tabBarVisible: false,
    menuVisible: false,
    goBackVisible: true,
    component: CreateEditBookingScreen,
    renderIcon: () => null
  }
};

export default function Router() {
  const { goBack } = useNavigation();

  const shouldRenderTabBar = (route: Partial<Route<string>>) => {
    const visibleScreenNames = Object.entries(ScreenConfigs)
      .filter(([key, screen]) => screen.tabBarVisible)
      .map(([key, screen]) => screen.name);
    return visibleScreenNames.indexOf(route.name as string) !== -1;
  };

  return (
    <Tab.Navigator
      backBehavior="history"
      initialRouteName={ScreenNames.BOOKING}>
      {Object.entries(ScreenConfigs).map(([key, screen]) => (
        <Tab.Screen
          key={screen.name}
          name={screen.name}
          component={screen.component as any}
          options={({ route }) => {
            return {
              tabBarVisible: screen?.menuVisible, //like this
              ...(!screen?.menuVisible && {
                tabBarButton: (props) => screen?.menuVisible
              }),
              headerLeft: () =>
                screen?.goBackVisible ? <BackButton onPress={goBack} /> : null,
              tabBarStyle: {
                display: shouldRenderTabBar(route) ? 'flex' : 'none'
              },
              tabBarIcon: ({ focused }) => {
                return screen.renderIcon({ focused });
              }
            };
          }}
        />
      ))}
    </Tab.Navigator>
  );
}
