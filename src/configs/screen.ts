export const ScreenNames = {
  BOOKING: 'Booking',
  BOOKING_DETAIL: 'Booking Detail',
  USER_PROFILE: 'My Profile',
  CREATE_BOOKING: 'Create Booking',
  EDIT_BOOKING: 'Edit Booking'
};
