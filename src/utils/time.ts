export const TimeUtils = {
  sleep: (ms: number) => {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
};
