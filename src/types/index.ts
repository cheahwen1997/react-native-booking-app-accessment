export interface AuthResponse {
  token?: string;
  reason?: string;
}

export interface BookingId {
  bookingid?: number;
}

export interface BookingDetail extends BookingId {
  firstname?: string;
  lastname?: string;
  totalprice?: number;
  depositpaid?: boolean;
  bookingdates?: {
    checkin?: string;
    checkout?: string;
  };
  additionalneeds?: string;
}

export interface ApiError {
  errorMessage?: string;
  statusCode?: string;
}

export interface GetBookingIdsFilter {
  firstname?: string;
  lastname?: string;
  checkin?: string;
  checkout?: string;
}

export interface CreateBookingResponse {
  bookingid: number;
  booking: BookingDetail;
}
