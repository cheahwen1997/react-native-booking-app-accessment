import * as axios from 'axios';
import {
  ApiError,
  AuthResponse,
  BookingDetail,
  BookingId,
  CreateBookingResponse,
  GetBookingIdsFilter
} from '../types';

export const BookingRestEndpoint = 'https://restful-booker.herokuapp.com';
export const BookingRestAuthUsername = 'admin';
export const BookingRestAuthPassword = 'password123';

const BookingRestClientConfig: axios.CreateAxiosDefaults = {
  baseURL: BookingRestEndpoint,
  timeout: 50000
};
class BookingRestClient {
  private static instance: BookingRestClient;
  private readonly _client: axios.Axios;

  constructor() {
    const instance = axios.default.create(BookingRestClientConfig);
    instance.defaults.headers.common['Accept'] = 'application/json';
    instance.interceptors.request.use(this.logRequest);
    instance.interceptors.response.use(this.onSuccess, this.onError);

    this._client = instance;
  }

  public static bootstrap(): BookingRestClient {
    if (!this.instance) {
      this.instance = new BookingRestClient();
    }
    return this.instance;
  }

  onSuccess(resp: axios.AxiosResponse) {
    console.log(
      `${BookingRestClient.name} Response::<Status ${resp?.status}> ${resp?.config?.baseURL}${resp?.config.url}`
    );
    return resp.data;
  }

  onError(err: axios.AxiosError): Promise<ApiError> {
    console.log(
      `${BookingRestClient.name} Response::<Status: ${err.response?.status}> ${err.response?.data}`
    );
    // alert(`Status Code Error: ${err.response?.data}` || "Api failed");
    return Promise.reject({
      errorMessage: err.response?.data,
      statusCode: err.response?.status
    });
  }

  logRequest(data: axios.InternalAxiosRequestConfig<any>) {
    console.log(
      `${BookingRestClient.name} Request::<${data.method?.toUpperCase()}> ${
        data.baseURL
      }${data.url} (Headers: ${data.headers})`
    );
    return data;
  }

  getToken(
    username: string | undefined = BookingRestAuthUsername,
    password: string | undefined = BookingRestAuthPassword
  ): Promise<AuthResponse> {
    return this._client
      .post('/auth', {
        username,
        password
      })
      .then((resp: AuthResponse | any) => {
        if (resp?.token) {
          this._client.defaults.headers.common['Cookie'] =
            `token=${resp.token}`;
        }
        return resp;
      });
  }

  getBookingIds(
    data?: GetBookingIdsFilter
  ): Promise<Array<BookingId> | undefined> {
    if (data && Object.keys(data).length > 0)
      return this._client.get(
        `/booking?${Object.entries(data)
          .map(([k, v]) => `${k}=${v}`)
          .join('&')}`
      );
    return this._client.get('/booking');
  }

  getBookingDetail(id: number): Promise<BookingDetail | undefined> {
    return this._client.get(`/booking/${id}`);
  }

  createBooking(
    data: BookingDetail
  ): Promise<CreateBookingResponse | undefined> {
    return this._client.post('/booking', data);
  }

  updateBooking(
    id: number,
    data: BookingDetail
  ): Promise<BookingDetail | undefined> {
    return this.getToken().then((resp) => {
      if (resp?.token)
        return this._client.patch(`/booking/${id}`, data, {
          headers: {
            Cookie: `token=${resp.token}`
          }
        });
    });
  }

  deleteBooking(id: number): Promise<string | undefined> {
    return this.getToken().then(({ token }) => {
      if (token)
        return this._client.delete(`/booking/${id}`, {
          headers: {
            Cookie: `token=${token}`
          }
        });
    });
  }
}

export default BookingRestClient;
