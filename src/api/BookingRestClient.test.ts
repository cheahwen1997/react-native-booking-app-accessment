import { expectTypeOf } from 'expect-type';
import { AuthResponse, BookingId } from '../types';
import { TimeUtils } from '../utils';
import BookingRestClient from './BookingRestClient';

jest.mock('../types');

describe('BookingRestClient', () => {
  let instance: BookingRestClient;

  beforeEach(() => {
    instance = BookingRestClient.bootstrap();
  });

  it('should be able to retrieve token', async () => {
    const resp = await instance.getToken();

    expectTypeOf(resp).toEqualTypeOf<AuthResponse>();
    expectTypeOf(resp.token).toEqualTypeOf<string | undefined>();
  });

  it('should not be able to retrieve token', async () => {
    const resp = await instance.getToken('test', 'test');

    expect(resp.token).toBe(undefined);
    expectTypeOf(resp.reason).toEqualTypeOf<string | undefined>();
    expect(resp.reason).toBe('Bad credentials');
  });

  it('response should match type BookingId[]', async () => {
    const bookingIds = await instance.getBookingIds();
    expectTypeOf(bookingIds).toEqualTypeOf<BookingId[] | undefined>();
  });

  it('response should return 404 error from getBookingIds', async () => {
    const bookingIds = await instance.getBookingIds({
      firstname: 'firstname=404',
      lastname: 'lastname=404'
    });
    expect(bookingIds).toHaveLength(0);
  });

  it('should not be able to get booking with invalid id', async () => {
    const bookingDetail = await instance.getBookingDetail(-1).catch((err) => {
      expect(err.errorMessage).toBe('Not Found');
      expect(err.statusCode).toBe(404);
    });
  });

  it('should return 200 to create a booking with valid fields', async () => {
    const mockBooking = {
      firstname: 'created',
      lastname: 'created',
      depositpaid: false,
      bookingdates: {
        checkin: '2024-04-07',
        checkout: '2024-04-20'
      },
      totalprice: 11,
      additionalneeds: 'test notes'
    };
    const createdBooking = await instance.createBooking(mockBooking);
    expect(createdBooking?.booking.firstname).toBe(mockBooking.firstname);
    expect(createdBooking?.booking.lastname).toBe(mockBooking.lastname);
    expect(createdBooking?.booking.depositpaid).toBe(mockBooking.depositpaid);
    expect(createdBooking?.booking.totalprice).toBe(mockBooking.totalprice);
    expect(createdBooking?.booking.bookingdates?.checkin).toBe(
      mockBooking.bookingdates.checkin
    );
    expect(createdBooking?.booking.bookingdates?.checkout).toBe(
      mockBooking.bookingdates.checkout
    );
    expect(createdBooking?.booking.additionalneeds).toBe(
      mockBooking.additionalneeds
    );
  });

  it('should return 500 to create booking for imcomplete fields', async () => {
    await instance.createBooking({}).catch((err) => {
      expect(err?.errorMessage).toBe('Internal Server Error');
      expect(err?.statusCode).toBe(500);
    });
  });

  it('should return 405 to update a non-existing booking id', async () => {
    await instance.updateBooking(-1, {}).catch((err) => {
      expect(err?.errorMessage).toBe('Method Not Allowed');
      expect(err?.statusCode).toBe(405);
    });
  });

  it('should return 405 to delete a non-existing booking id', async () => {
    await instance.deleteBooking(-1).catch((err) => {
      expect(err?.errorMessage).toBe('Method Not Allowed');
      expect(err?.statusCode).toBe(405);
    });
  });

  it('should return 201 to delete a non-existing booking id', async () => {
    const createdBooking = await instance.createBooking({
      firstname: 'to be deleted',
      lastname: 'to be deleted',
      depositpaid: false,
      bookingdates: {
        checkin: '2024-04-07',
        checkout: '2024-04-20'
      },
      totalprice: 222
    });

    await TimeUtils.sleep(1000);

    const deletedBooking = await instance.deleteBooking(
      createdBooking?.bookingid as number
    );
    expect(deletedBooking).toBe('Created');
  });
});
