import { Text, View } from 'react-native';

export default function BadgeID({ label }: { label: any }) {
  return (
    <View
      className={
        'bg-yellow-100 me-2 px-2.5 py-0.5 rounded-full dark:bg-yellow-900 dark:text-yellow-300'
      }>
      <Text className="text-yellow-800 text-xs font-medium">{label}</Text>
    </View>
  );
}
