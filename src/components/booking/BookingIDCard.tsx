import { useNavigation } from '@react-navigation/native';
import { Text, TouchableOpacity, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScreenNames } from '../../configs';
import { BookingDetail } from '../../types';
import Card from '../Card';
import FlatButton from '../FlatButton';

export default function BookingIDCard({
  booking,
  isEditable = false,
  isDeletable = false,
  onEdit = () => {},
  onDelete = () => {}
}: {
  booking: BookingDetail;
  isEditable?: boolean;
  isDeletable?: boolean;
  onEdit?: () => void;
  onDelete?: () => void;
}) {
  const navigation = useNavigation();

  return (
    <View className="m-1" key={booking.bookingid}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate({
            name: ScreenNames.BOOKING_DETAIL,
            params: { id: booking.bookingid }
          } as never);
        }}>
        <Card>
          <View className="flex flex-row justify-between items-center">
            <Text className="text-neutral-400">ID: {booking.bookingid}</Text>
            <View className="flex flex-row space-x-3 justify-evenly">
              {isEditable && (
                <View>
                  <FlatButton
                    onPress={() => {
                      onEdit();
                      navigation.navigate({
                        name: ScreenNames.EDIT_BOOKING,
                        params: {
                          id: booking.bookingid
                        }
                      } as never);
                    }}
                    buttonClassName="bg-green-500 p-1"
                    icon={
                      <MaterialIcons name="create" color="white" size={20} />
                    }
                  />
                </View>
              )}
              {isDeletable && (
                <View>
                  <FlatButton
                    onPress={onDelete}
                    buttonClassName="bg-red-500 p-1"
                    icon={
                      <MaterialIcons name="delete" color="white" size={20} />
                    }
                  />
                </View>
              )}
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    </View>
  );
}
