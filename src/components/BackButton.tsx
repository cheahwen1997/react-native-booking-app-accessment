import { Text, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function BackButton({ onPress }: { onPress?: () => void }) {
  return (
    <TouchableOpacity
      className="flex flex-row justify-center items-center"
      onPress={onPress}>
      <Ionicons name="chevron-back" size={24} color="gray" />
      <Text className="text-slate-500"> Back </Text>
    </TouchableOpacity>
  );
}
