import { SafeAreaView } from 'react-native';

export default function BaseLayout({ children }: { children: any }) {
  return <SafeAreaView className="p-5 h-full w-full">{children}</SafeAreaView>;
}
