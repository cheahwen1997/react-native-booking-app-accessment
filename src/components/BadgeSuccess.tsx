import { Text, View } from 'react-native';

export default function BadgeError({ label }: { label: string }) {
  return (
    <View
      className={
        'bg-green-100 me-2 px-2.5 py-0.5 rounded-full dark:bg-green-900 dark:text-green-300'
      }>
      <Text className="text-green-800 text-xs font-medium">{label}</Text>
    </View>
  );
}
