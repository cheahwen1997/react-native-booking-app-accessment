import { Text, View } from 'react-native';

export default function BadgeError({
  label,
  className = ''
}: {
  label: string;
  className?: string;
}) {
  return (
    <View
      className={
        'bg-red-100 me-2 px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300' +
        className
      }>
      <Text className="text-red-800 text-xs font-medium">{label}</Text>
    </View>
  );
}
