import { TouchableOpacity, View } from 'react-native';

export default function FloatingActionButton({
  icon,
  onPress,
  disabled = false,
  buttonClassName = '',
  top,
  right,
  bottom,
  left
}: {
  icon: React.ReactElement;
  onPress: () => void;
  disabled?: boolean;
  buttonClassName?: string;
  top?: string;
  right?: string;
  bottom?: string;
  left?: string;
}) {
  return (
    <View
      className={`absolute top-${top} right-${right} bottom-${bottom} left-${left}`}>
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <View className={'bg-sky-500 p-2 rounded ' + buttonClassName}>
          {icon}
        </View>
      </TouchableOpacity>
    </View>
  );
}
