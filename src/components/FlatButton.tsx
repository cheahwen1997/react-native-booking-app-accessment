import { Text, TouchableOpacity, View } from 'react-native';

export default function FlatButton({
  onPress = () => {},
  disabled = false,
  icon,
  label = '',
  buttonClassName = '',
  labelClassName = ''
}: {
  onPress?: () => void;
  disabled?: boolean;
  icon?: React.ReactElement;
  label?: string;
  buttonClassName?: string;
  labelClassName?: string;
}) {
  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}>
      <View
        className={
          'flex flex-row space-x-2 justify-center items-center rounded ' +
          buttonClassName
        }>
        {label ? (
          <Text className={'text-white ' + labelClassName}>{label}</Text>
        ) : null}
        {icon}
      </View>
    </TouchableOpacity>
  );
}
