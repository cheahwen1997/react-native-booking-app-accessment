import { View } from 'react-native';

export default function Card({
  children,
  className = ''
}: {
  children: any;
  className?: string;
}) {
  return (
    <View className={'p-5 bg-neutral-800 rounded-md ' + className}>
      {children}
    </View>
  );
}
