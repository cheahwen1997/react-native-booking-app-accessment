import { ActivityIndicator, View } from 'react-native';

export default function LoadingOutline() {
  return (
    <View className="w-full h-full center">
      <ActivityIndicator size="large" />
    </View>
  );
}
