import { AxiosError } from 'axios';
import { useState } from 'react';

export const useRequest = <T>(api: () => void) => {
  const [data, setData] = useState<T>();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<AxiosError | null>(null);

  const callApi = async () => {
    setLoading(true);
    setError(null);
    try {
      let result = await api();

      setData(result as T);
      setError(null);
      setLoading(false);
      return true;
    } catch (error: any) {
      setError(error);
      setLoading(false);
      return false;
    }
  };

  return { data, loading, callApi, error };
};
