module.exports = {
  bracketSameLine: true,
  singleQuote: true,
  trailingComma: 'none',
  semi: true,
  endOfLine: 'lf',
  printWidth: 80,
  plugins: ['prettier-plugin-organize-imports']
};
