/** @type {import('tailwindcss').Config} */
/// <reference types="nativewind/types" />

module.exports = {
  content: ['./App.tsx', './src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {}
  },
  plugins: []
};
