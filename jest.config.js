module.exports = {
  preset: 'react-native',
  testEnvironment: 'node',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  coverageReporters: ['text', 'text-summary'],
  setupFiles: ['./jest-setup.js'],
  collectCoverage: false,
  transform: {
    '^.+\\.(ts|tsx|js|jsx)?$': '<rootDir>/node_modules/babel-jest'
  },
  transformIgnorePatterns: [
    'node_modules/(?!(jest-|@react-native|@react-native-community|react-native))'
  ]
};
