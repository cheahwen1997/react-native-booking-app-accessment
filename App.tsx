import { DarkTheme, NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import Router from './src/Router';

export default function App() {
  return (
    <NavigationContainer theme={DarkTheme}>
      <StatusBar />
      <Router />
    </NavigationContainer>
  );
}
